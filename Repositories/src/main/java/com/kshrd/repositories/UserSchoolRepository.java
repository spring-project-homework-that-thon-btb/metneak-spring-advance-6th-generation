package com.kshrd.repositories;


import com.kshrd.models.UserSchool;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface UserSchoolRepository extends JpaRepository<UserSchool,Integer> {




    @Modifying
    @Transactional
    @Query(value = "delete from mn_user_school where user_id=:id", nativeQuery = true)
    int deleteUserSchoolByUserId1(@Param("id") int id);


    @Override
    <S extends UserSchool> List<S> saveAll(Iterable<S> iterable);

    @Override
    <S extends UserSchool> S save(S s);

}
