package com.kshrd.repositories;

import com.kshrd.models.School;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Repository
public interface SchoolRepository extends JpaRepository<School,Integer> {



        @Query(value = "SELECT s.id,s.level,s.name,l.districtName,l.provinceName\n" +
                "FROM School s INNER JOIN Location l ON s.locations = l.id where s.status = true ")
        List<School> getall();


        @Query(value = "select s from School s where s.status=true order by s.name asc ")
        List<School> findAllSchools();



        @Modifying
        @Transactional
        @Query(value = "select us.school_id, name, level, graduated_year, s.status, created_date, modified_date\n" +
                "                           from mn_user_school us inner join mn_school s on us.school_id = s.id\n" +
                "                           where us.user_id =:userId",nativeQuery = true)
        List<School> findSchoolByUserId(@Param("userId") int userId);


        @Modifying
        @Transactional
        @Query(value = "select us.school,us.user.id, s.name, s.level, us.graduatedYear, s.status, us.createdDate, us.modifiedDate\n" +
                "                           from UserSchool us inner join School s on us.school = s.id\n" +
                "                     where us.user.id =  :userId    ")
        List<School> findSchoolByUserId1(@Param("userId") int userId);


        @Modifying
        @Transactional
        @Query(value = "insert into mn_user_school(school_id,user_id) values (:school_id,:userId)",nativeQuery = true)
        int saveUserSchools(@Param("school_id") int schoolList,@Param("userId") int userId);


}
