package com.kshrd.repositories;

import com.kshrd.models.OneSignalPushNotifcation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface OneSignalPushNotificationRepository extends JpaRepository<OneSignalPushNotifcation,Integer> {

    /**
     * save device of user used
     */
    @Modifying
    @Query(value = "INSERT INTO one_signal_push_notifcation(user_id,player_id) VALUES(:userId,:playerId)",nativeQuery = true)
    public Integer saveOneDevice(@Param("userId")Integer userId,@Param("playerId")String playerId);


    // confirm device
    public OneSignalPushNotifcation findOneSignalPushNotifcationByPlayerId(@Param("playerId")String playerId);

    @Query(value = "select o.player_id from one_signal_push_notifcation o where o.user_id=:userId",nativeQuery = true)
    public List<String> findOneSignalPushNotifcationByUserId(@Param("userId")Integer userId);


    @Query(value = "SELECT DISTINCT\n" +
            "o.player_id\n" +
            "FROM mn_user u\n" +
            "INNER JOIN mn_user_group g on u.id=g.user_id\n" +
            "INNER JOIN one_signal_push_notifcation o on o.user_id=u.id WHERE g.group_id=:gId",nativeQuery = true)
    public List<String> findOneSignalPushNotifcationByGroupId(@Param("gId") Integer gId);

    @Query(value = "SELECT DISTINCT\n" +
            "o.player_id\n" +
            "FROM mn_comment c \n" +
            "INNER JOIN mn_user u on u.id=c.user_id\n" +
            "INNER JOIN one_signal_push_notifcation o on o.user_id=u.id\n" +
            "INNER JOIN mn_post p on p.id=c.post_id\n" +
            "where c.post_id=:postId or p.user_id=:userId;",nativeQuery = true)
    public List<String> findOneSignalPushNotifcationForComment(@Param("postId")Integer postId,@Param("userId")Integer userId);


}
