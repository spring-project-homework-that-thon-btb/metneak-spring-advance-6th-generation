package com.kshrd;



import com.kshrd.models.Location;
import com.kshrd.models.User;
import com.kshrd.repositories.UserRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import java.sql.Timestamp;


@SpringBootApplication
public class WebServiceApplication extends SpringBootServletInitializer implements ApplicationRunner {


    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {

        return builder.sources(WebServiceApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(WebServiceApplication.class, args);
    }


    public WebServiceApplication(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    private UserRepository userRepository;


    @Override
    public void run(ApplicationArguments args) throws Exception {

    }
}
