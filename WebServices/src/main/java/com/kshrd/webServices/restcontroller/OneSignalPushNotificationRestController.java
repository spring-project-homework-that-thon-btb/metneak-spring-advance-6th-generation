package com.kshrd.webServices.restcontroller;

import com.kshrd.models.OneSignalPushNotifcation;
import com.kshrd.services.OneSignalPushNotificationService;
import com.kshrd.webServices.config.PushNotificationOption;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/push")
public class OneSignalPushNotificationRestController {

    private OneSignalPushNotificationService oneSignalPushNotificationService;

    private Logger logger = LogManager.getLogger();

    @Autowired
    public void setOneSignalPushNotificationService(OneSignalPushNotificationService oneSignalPushNotificationService) {
        this.oneSignalPushNotificationService = oneSignalPushNotificationService;
    }

    // save Devices
    @PostMapping("/save/{userId}/{playerId}")
    public Map<String, Object> saveDevice(@PathVariable("userId") Integer userId, @PathVariable("playerId") String playerId) {
        logger.info(playerId);
        Map<String, Object> response = new HashMap<>();
        Integer i = oneSignalPushNotificationService.saveOneDevice(userId, playerId);
        if (i < 0) {
            response.put("status", false);
            response.put("message", "device fail to insert");
            response.put("execute", i);
            return response;
        }
        response.put("status", true);
        response.put("message", "device insert success");
        response.put("execute", i);
        return response;
    }

    @PostMapping("/{playerId}")
    public Map<String, Object> checkExistingDevice(@PathVariable("playerId") String playerId) {
        logger.info(playerId);
        Map<String, Object> response = new HashMap<>();
        OneSignalPushNotifcation oneSignalPushNotifcation = oneSignalPushNotificationService.findOneDevice(playerId);
        if (oneSignalPushNotifcation == null) {
            response.put("status", false);
            response.put("message", "device don't have one");
            response.put("data", oneSignalPushNotifcation);
            return response;
        }
        response.put("status", true);
        response.put("message", "device have one");
        response.put("data", oneSignalPushNotifcation);
        return response;
    }


    @PostMapping("/sendMessageToAllUsers/{message}")
    public void sendMessageToAllUsers(@PathVariable("message") String message) {
        PushNotificationOption.sendMessageToAllUsers(message);
    }


    @PostMapping("/sendMessageToUser/{userId}/{message}")
    public void sendMessageToUserById(@PathVariable("userId") Integer userId,
                                      @PathVariable("message") String message) {
        List<String> deviceOfUser = oneSignalPushNotificationService.findDeviceByUserId(userId);
        logger.info(deviceOfUser);
        PushNotificationOption.sendMessageToUser(message, deviceOfUser);
    }


    @PostMapping("/sendMessageToUserInGroup/{gId}/{message}")
    public void sendMessageToUserByGroupId(@PathVariable("gId") Integer gId,
                                           @PathVariable("message") String message) {
        logger.info(gId);
        List<String> allDevice = oneSignalPushNotificationService.findOneSignalPushNotifcationByGroupId(gId);
        PushNotificationOption.sendMessageToUser(message, allDevice);
    }

    @PostMapping("/sendMessageToUserInGroup/{ownerId}/{msgOwner}/{receiveId}/{msgReceived}")
    public void sendMessageToUserWhenMention(@PathVariable("ownerId") Integer ownerId,
                                             @PathVariable("receiveId") Integer received,
                                             @PathVariable("msgOwner") String msgOwner,
                                             @PathVariable("msgReceived") String msgReceived) {
        //your post have mention by other
        List<String> allDeviceOwner = oneSignalPushNotificationService.findDeviceByUserId(ownerId);
        PushNotificationOption.sendMessageToUser(msgOwner, allDeviceOwner);

        // user mention to see post
        List<String> allDeviceReceive = oneSignalPushNotificationService.findDeviceByUserId(received);
        PushNotificationOption.sendMessageToUser(msgOwner, allDeviceReceive);

    }

    @PostMapping("/sendMessageToUserComment/{postId}/{userId}/{message}")
    public void pushToUserWhenComment(@PathVariable("postId") Integer postId,
                                      @PathVariable("userId") Integer userId,
                                      @PathVariable("message") String message) {
        //your post have mention by other
        List<String> allDeviceOwner = oneSignalPushNotificationService.findOneSignalPushNotifcationForComment(postId, userId);
        PushNotificationOption.sendMessageToUser(message, allDeviceOwner);

    }




}
