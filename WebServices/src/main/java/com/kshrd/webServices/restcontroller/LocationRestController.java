package com.kshrd.webServices.restcontroller;


import com.kshrd.models.Location;
import com.kshrd.services.LocationService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/location")
public class LocationRestController {



    private LocationService locationService;

    public LocationRestController(LocationService locationService) {
        this.locationService = locationService;
    }


    @GetMapping("/getallprovince")
    public Map<String,Object> getallprovince()
    {
        Map<String,Object> map = new HashMap<>();
        List<Location> locations = this.locationService.getallprovince();
        if(!locations.isEmpty())
        {
            map.put("Status",true);
            map.put("Data",locations);
            map.put("Message","Get Data Successfully");

        }else
        {
            map.put("Status",false);
            map.put("Message","Get Data Unsuccessfully");
        }
        return map;
    }


    @GetMapping("/findDistrictName")
    public Map<String,Object> findDistrictName(@RequestParam String provinceName)
    {
        Map<String,Object> map = new HashMap<>();
        List<Location> findDistrictName = this.locationService.findDistrictNameByProvinceName(provinceName);
        if(!findDistrictName.isEmpty())
        {
            map.put("Status",true);
            map.put("Data",findDistrictName);
            map.put("Message","Get Data Successfully");

        }else
        {
            map.put("Status",false);
            map.put("Message","Get Data Unsuccessfully");
        }
        return map;
    }


}
