package com.kshrd.webServices.restcontroller;

import com.kshrd.models.Pagination;
import com.kshrd.models.Post;
import com.kshrd.services.PostService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.PostUpdate;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/post")
public class PostRestController {

    private PostService postService;

    private Logger logger = LogManager.getLogger();

    @Autowired
    public void setPostService(PostService postService) {
        this.postService = postService;
    }


    /**
     * 1. savePostInGroup
     */
    //        request type
//        {
////            "content":"I love you so much",
////                "photoUrl":"This is pohoto for you",
////                "status":"true",
////                "uId":"1",
////                "gId":"1"
////        }
//    @PostMapping(value = "/save",produces = { "application/json" })
    @RequestMapping(value = "/save",produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
//    @PostMapping(value = "/save",produces = { "application/json" })
    public Map<String, Object> savePostInGroup(@RequestBody Post post) {

        logger.info(post);

        System.out.println("POST: "+post.getContent()+", "+post.getPhotoUrl()+", "+post.getuId()+", "+post.getgId());
        Map<String, Object> response = new HashMap<>();
        Integer i = postService.savePostInGroup(post);
        if (i < 0) {
            response.put("status", false);
            response.put("message", "no operation");
            response.put("execute", i);
            return response;
        }
        response.put("status", true);
        response.put("message", "success");
        response.put("execute", i);
        return response;
    }


    /**
     * 2. findPostsByGroupId
     */
    @GetMapping("/group")
    public Map<String, Object> findPostsByGroupId(
            @RequestParam(defaultValue = "1", value = "groupId", required = false) Integer groupId,
            @RequestParam(defaultValue = "1", value = "page", required = false) Integer page,
            @RequestParam(defaultValue = "12", value = "limit", required = false) Integer limit
    ) {
        Pageable pageable = new PageRequest(page, limit, Sort.Direction.DESC, "id");
        Page<Post> allPosts = postService.findPostsByGroupId(groupId, pageable);
        Pagination pagination = new Pagination(allPosts.getPageable().getPageNumber(), limit, allPosts.getTotalElements(), allPosts.getTotalPages());
        Map<String, Object> res = new HashMap<>();
        if (allPosts.getContent().isEmpty()) {
            res.put("status", false);
            res.put("message", "no data");
            return res;
        }
        res.put("status", true);
        res.put("message", "Get Data Successfully");
        res.put("data", allPosts.getContent());
        res.put("pagination", pagination);
        return res;
    }


    /**
     * 3. findPostsByAllGroupId
     */
    @GetMapping("/all-group")
    public ResponseEntity<Map<String, Object>> findPostsByAllGroupId(
            @RequestParam(defaultValue = "1", value = "g1", required = false) Integer g1,
            @RequestParam(defaultValue = "2", value = "g2", required = false) Integer g2,
            @RequestParam(defaultValue = "3", value = "g3", required = false) Integer g3,
            @RequestParam(defaultValue = "0", value = "page", required = false) Integer page,
            @RequestParam(defaultValue = "12", value = "limit", required = false) Integer limit
    ) {

        Pageable pageable = new PageRequest(page, limit, Sort.Direction.DESC, "id");
        Map<String, Object> res = new HashMap<>();
        Page<Post> allPosts = postService.findPostsByAllGroupId(g1, g2, g3, pageable);
        Pagination pagination = new Pagination(allPosts.getPageable().getPageNumber(), limit, allPosts.getTotalElements(), allPosts.getTotalPages());
        if (allPosts.getContent().isEmpty()) {
            res.put("status", false);
            res.put("message", "no data");
            return new ResponseEntity(res, HttpStatus.NOT_FOUND);
        }
        res.put("status", true);
        res.put("message", "Get Data Successfully");
        res.put("data", allPosts.getContent());
        res.put("pagination", pagination);
        return new ResponseEntity(res, HttpStatus.OK);
    }


    /**
     * 4. countPostByGroupId
     */

    @GetMapping("/count/{g}")
    public Map<String, Object> countPostByGroupId(@PathVariable("g") Integer g) {
        Map<String, Object> response = new HashMap<>();
        response.put("status", true);
        response.put("message", "success");
        response.put("data", postService.countPostByGroupId(g, true));
        return response;
    }


    /**
     * 5. countPostByAllGroupId
     */
    @GetMapping("/count/group/g1/{g1}/g2/{g2}/g3/{g3}")
    public Map<String, Object> countPostByAllGroup(@PathVariable("g1") Integer g1, @PathVariable("g2") Integer g2, @PathVariable("g3") Integer g3) {
        Map<String, Object> response = new HashMap<>();
        response.put("status", true);
        response.put("message", "success");
        response.put("data", postService.countPostByAllGroup(g1, g2, g3));
        return response;
    }


    /**
     * 6. findAllPosts
     */
    @GetMapping("/all")
    public List<Post> findAllPost() {
        Map<String, Object> res = new HashMap<>();
        return postService.findAllPost();
    }

    /**
     * 7. findPostsByUserId
     */
    @GetMapping("/user")
    public ResponseEntity<Map<String, Object>> findPostByUserId(
            @RequestParam(defaultValue = "1", value = "uid", required = false) Integer uid,
            @RequestParam(defaultValue = "0", value = "page", required = false) Integer page,
            @RequestParam(defaultValue = "12", value = "limit", required = false) Integer limit
    ) {
        Map<String, Object> response = new HashMap<>();
        Pageable pageable = new PageRequest(page, limit, Sort.Direction.DESC, "id");
        Page<Post> allPosts = postService.findPostByUserId(uid, pageable);
        Pagination pagination = new Pagination(allPosts.getPageable().getPageNumber(), limit, allPosts.getTotalElements(), allPosts.getTotalPages());
        if (allPosts.getContent().isEmpty()) {
            response.put("status", false);
            response.put("message", "no data");
            return new ResponseEntity(response, HttpStatus.NOT_FOUND);
        }
        response.put("status", true);
        response.put("message", "success");
        response.put("data", allPosts.getContent());
        response.put("pagination", pagination);
        return new ResponseEntity(response, HttpStatus.OK);
    }

    /**
     * 8. countPostsByUserId
     */
    @GetMapping("/user/count/{uid}")
    public Map<String, Object> countPostByUserId(@PathVariable("uid") Integer uid) {
        Map<String, Object> response = new HashMap<>();
        response.put("status", true);
        response.put("message", "success");
        response.put("data", postService.countPostByUserId(uid));
        return response;
    }


    /**
     * 9. findPostById
     */
    @GetMapping("/{id}")
    public Map<String, Object> findPostById(@PathVariable("id") Integer id) {
        Map<String, Object> response = new HashMap<>();
        Post post = postService.findPostById(id, true);
        if (post == null) {
            response.put("status", false);
            response.put("message", "Not found!");
            return response;
        }
        response.put("status", true);
        response.put("message", "success");
        response.put("data", post);
        return response;
    }

    /**
     * 10. filterPostsByGroupId Date and status
     */
    @GetMapping("/filter")
    public ResponseEntity<Map<String, Object>> filterPostsByGroupIdAndDate(
            @RequestParam(value = "start", required = false) String start,
            @RequestParam(value = "end", required = false) String end,
            @RequestParam(defaultValue = "1", value = "gId", required = false) Integer gId,
            @RequestParam(defaultValue = "0", value = "page", required = false) Integer page,
            @RequestParam(defaultValue = "12", value = "limit", required = false) Integer limit
    ) {

        System.out.println("G: " + gId);
        System.out.println("S: " + start);
        System.out.println("E: " + end);

        Map<String, Object> response = new HashMap<>();
        Pageable pageable = new PageRequest(page, limit, Sort.Direction.DESC, "id");
        Page<Post> allPosts = postService.filterPostsByGroupId(gId, start, end, true, pageable);
        Pagination pagination = new Pagination(allPosts.getPageable().getPageNumber(), limit, allPosts.getTotalElements(), allPosts.getTotalPages());
        if (allPosts.getContent().isEmpty()) {
            response.put("status", false);
            response.put("message", "no data");
            return new ResponseEntity(response, HttpStatus.NOT_FOUND);
        }
        response.put("status", true);
        response.put("message", "Get Data Successfully");
        response.put("data", allPosts.getContent());
        response.put("pagination", pagination);
        return new ResponseEntity(response, HttpStatus.OK);
    }


    /**
     * 11. findPostsByMateId
     */
    @GetMapping("/mate")
    public ResponseEntity<Map<String, Object>> findPostByMateId(
            @RequestParam(defaultValue = "1", value = "mid", required = false) Integer mid,
            @RequestParam(defaultValue = "0", value = "page", required = false) Integer page,
            @RequestParam(defaultValue = "12", value = "limit", required = false) Integer limit
    ) {
        Map<String, Object> response = new HashMap<>();
        Pageable pageable = new PageRequest(page, limit, Sort.Direction.DESC, "id");
        Page<Post> allPosts = postService.findPostByMateId(mid, true, pageable);
        Pagination pagination = new Pagination(allPosts.getPageable().getPageNumber(), limit, allPosts.getTotalElements(), allPosts.getTotalPages());
        if (allPosts.getContent().isEmpty()) {
            response.put("status", false);
            response.put("message", "no data");
            return new ResponseEntity(response, HttpStatus.NOT_FOUND);
        }
        response.put("status", true);
        response.put("message", "Get Data Successfully");
        response.put("data", allPosts.getContent());
        response.put("pagination", pagination);
        return new ResponseEntity(response, HttpStatus.OK);
    }

    /**
     * 11. findPostsByMateId
     */
    //        request type
//        {
//            "content":"I love you so much",
//                "photoUrl":"This is pohoto for you",
//                "status":"true",
//                "mateId":"1",
//                "uId":"1"
//        }
    @PostMapping("/mate/save")
    public Map<String, Object> savePostForMate(@RequestBody Post post) {
        Map<String, Object> response = new HashMap<>();
        Integer b = postService.savePostForMate(post);
        if (b > 0) {
            response.put("status", true);
            response.put("message", "success");
            response.put("excute", b);
            return response;
        }
        response.put("status", false);
        response.put("message", "no operation");
        response.put("excute", b);
        return response;
    }

    /**
     * delete Post  by Id
     */
    @DeleteMapping("/{id}")
    public Map<String, Object> deletePostById(@PathVariable("id") Integer id) {
        System.out.println("Update ID: "+id);
        Integer b=postService.deletePostById(id);
        Map<String, Object> res = new HashMap<>();
        if(b>0){
            res.put("status", true);
            res.put("message", "completed!");
            res.put("excute", b);
            return res;
        }
        res.put("status", false);
        res.put("message", "no operation");
        res.put("excute", b);
        return res;
    }


//    something wrong with update for and
//    request type
//{
//         "id":"2",
//        "content":"I love you so much",
//        "photoUrl":"This is pohoto for you"
//}
    @PutMapping("/update")
    public Map<String, Object> updatePostById(@RequestBody Post post){
        Map<String, Object> res = new HashMap<>();
        Integer i=postService.updatePostById(post);
        if(i>0){
            res.put("status",true);
            res.put("message","complete");
            res.put("execute",i);
            return res;
        }
        res.put("status",false);
        res.put("message","no operations");
        res.put("execute",i);
        return res;
    }



}
