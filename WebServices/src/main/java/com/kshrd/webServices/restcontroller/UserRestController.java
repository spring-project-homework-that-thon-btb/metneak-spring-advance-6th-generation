package com.kshrd.webServices.restcontroller;


import com.kshrd.models.School;
import com.kshrd.models.User;
import com.kshrd.models.from.FrmsocailMedia;
import com.kshrd.repositories.UserRepository;
import com.kshrd.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/user")

public class UserRestController {




    private UserService userService;
    private UserRepository userRepository;

    public UserRestController(UserService userService, UserRepository userRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
    }


    @GetMapping(value = "/getall",produces = {"application/json"})
    public Map<String,Object> getalluser()
    {
        Map<String,Object> map = new HashMap<>();
        List<User> user = this.userService.getall();

        if(!user.isEmpty())
        {
            map.put("Status",true);
            map.put("Data",user);
            map.put("Message","Get Data Successfully");

        }else
        {
            map.put("Status",false);
            map.put("Message","Get Data Unsuccessfully");
        }
        return map;
    }


    @GetMapping(value = "/findAllUserFriendByUserId/{userId}",produces = {"application/json"})
    public Map<String,Object> findAllUserFriendByUserId(@PathVariable("userId") int userId)
    {
        Map<String,Object> map = new HashMap<>();
        List<User> user = this.userService.findAllUserFriendByUserId(userId);

        if(!user.isEmpty())
        {
            map.put("Status",true);
            map.put("Data",user);
            map.put("Message","Get Data Successfully");

        }else
        {
            map.put("Status",false);
            map.put("Message","Get Data Unsuccessfully");
        }
        return map;
    }




    @PostMapping("/notification/insert/{userId}/{notificationId}")
    public Map<String,Object> insertUserReadNotification(@PathVariable("userId") int userId,@PathVariable("notificationId") int notificationId)
    {
        List user = userService.insertuserreadnotification(userId,notificationId);
        Map<String,Object> map = new HashMap<>();
        if (!user.isEmpty())
        {
            map.put("Status",true);
            map.put("Data",user);
            map.put("Message","Get Data Successfully");
        }
        else
        {
            map.put("Status",false);
            map.put("Message","Get Data Unsuccessfully");
        }

        return map;

    }


    @PutMapping("/update_user")
    public Map<String,Object> updateUser(@RequestBody User user, HttpServletRequest request)
    {
//        User u = (User) request.getSession().getAttribute("user");
//        user.setId(u.getId());

        Map<String,Object> map = new HashMap<>();
        if (userService.updateUserById(user))
        {
            map.put("Status",true);
            map.put("Message","Update Data Successfully");
        }
        else
        {
            map.put("Status",false);
            map.put("Message","Update Data Unsuccessfully");
        }

        return map;

    }


    @GetMapping(value = "/findUserById/{userId}",produces = {"application/json"})
    public Map<String,Object> findUserById(@PathVariable("userId") int userId)
    {
        Map<String,Object> map = new HashMap<>();
        List<User> user = this.userService.findUserById(userId);

        if(!user.isEmpty())
        {
            map.put("Status",true);
            map.put("Data",user);
            map.put("Message","Get Data Successfully");

        }else
        {
            map.put("Status",false);
            map.put("Message","Get Data Unsuccessfully");
        }
        return map;
    }

    @GetMapping("/remember/{userId}/{mateId}/{status}")
    public List<String> insertUpdateRemember
            (
                    @PathVariable("userId") int userId,
                    @PathVariable("mateId") int mateId,
                    @PathVariable("status") boolean status
            )
    {
        List<String> s = new ArrayList<>();
        s.add(String.valueOf(userService.insertUpdateRemember(userId, mateId, status)));
        return s;
    }

    @GetMapping("/checkRemember/{userId}/{mateId}")
    public List<String> checkUserAndMateById(@PathVariable("userId") int userId, @PathVariable("mateId") int mateId, HttpServletRequest request){

        User u = (User) request.getSession().getAttribute("user");
        userId = u.getId();

        List<String> s = new ArrayList<>();
        int count = userService.checkuserandmatebyid(userId, mateId);
        if(count == 0)
            s.add("false");
        else
            s.add("true");
        return s;
    }

    @PostMapping("/save")
    public Map<String,Object> saveUser(@RequestBody User user)
    {

        System.out.println(user);

        Map<String,Object> map = new HashMap<>();
        if (userService.saveuser(user))
        {
            map.put("Status",true);
            map.put("Message","Insert Data Successfully");
        }
        else {
            map.put("Status",false);
            map.put("Message","Insert Data UnSuccessfully");
        }
        return map;
    }


    @PostMapping("/saveUserMedia")
    public Map<String,Object> saveUserMedia(@RequestBody FrmsocailMedia frmsocailMedia)
    {
        Map<String,Object> map = new HashMap<>();
        if (userService.saveUserSocailmedia(frmsocailMedia))
        {
            map.put("Status",true);
            map.put("Message","Insert Data Successfully");
        }
        else {
            map.put("Status",false);
            map.put("Message","Insert Data UnSuccessfully");
        }
        return map;
    }

    @PutMapping("/register")
    public Map<String, Object> registerUser(@RequestBody User user) {
        Map<String,Object> map = new HashMap<>();
        if (userService.registerUser(user))
        {
            map.put("Status",true);
            map.put("Message","Insert Data Successfully");
        }
        else
        {
            map.put("Status",false);
            map.put("Message","Insert Data UnSuccessfully");
        }
        return map;
    }



    @PostMapping("/{facebookId}")
    public Map<String,Object> findUserByFacebookId(@PathVariable("facebookId") String facebookId)
    {
        List<User> users = userService.findUserByFacebookId(facebookId);
        Map<String,Object> map = new HashMap<>();
        if (!users.isEmpty())
        {
            map.put("Status",true);
            map.put("Message","Data Successfully");
        }
        else {
            map.put("Status",true);
            map.put("Message","Data UnSuccessfully");
        }
        return map;
    }








































































    @GetMapping(value = "/countAllUser",produces = {"application/json"})
    public Map<String,Object> countAllUser()
    {
        Map<String,Object> map = new HashMap<>();
        int user = this.userService.countAllUser();


            map.put("Status",true);
            map.put("Data",user);
            map.put("Message","Get Data Successfully");


        return map;
    }


    @GetMapping(value = "/findUserByFacebookId",produces = {"application/json"})
    public Map<String,Object> findSchoolByUserId(@RequestParam String facebookId)
    {
        Map<String,Object> map = new HashMap<>();
        List<User> user = this.userService.findUserByFacebookId(facebookId);

        if(!user.isEmpty())
        {
            map.put("Status",true);
            map.put("Data",user);
            map.put("Message","Get Data Successfully");

        }else
        {
            map.put("Status",false);
            map.put("Message","Get Data Unsuccessfully");
        }
        return map;
    }

//    @GetMapping(value = "/findAllByEmail",produces = {"application/json"})
//    public Map<String,Object> findAllByEmail(@RequestParam String email)
//    {
//        Map<String,Object> map = new HashMap<>();
//        List<User> user = this.userService.findAllByEmail(email);
//
//        if(!user.isEmpty())
//        {
//            map.put("Status",true);
//            map.put("Data",user);
//            map.put("Message","Get Data Successfully");
//
//        }else
//        {
//            map.put("Status",false);
//            map.put("Message","Get Data Unsuccessfully");
//        }
//        return map;
//    }




    @GetMapping(value = "/countAllUserFriend/{user_id}",produces = {"application/json"})
    public Map<String,Object> countAllUserFriend(@PathVariable("user_id") int user_id)
    {
        Map<String,Object> map = new HashMap<>();
        int user = this.userService.countAllUserFriend(user_id);

        if(user>0)
        {
            map.put("Status",true);
            map.put("Data",user);
            map.put("Message","Get Data Successfully");

        }else
        {
            map.put("Status",false);
            map.put("Message","Get Data Unsuccessfully");
        }
        return map;
    }



    @GetMapping(value = "/findAllUserInGroup/{groupId}",produces = {"application/json"})
    public Map<String,Object> findAllUserInGroup(@PathVariable("groupId") int groupId)
    {
        Map<String,Object> map = new HashMap<>();
        List<User> user = this.userService.findAllUserInGroup(groupId);

        if(!user.isEmpty())
        {
            map.put("Status",true);
            map.put("Data",user);
            map.put("Message","Get Data Successfully");

        }else
        {
            map.put("Status",false);
            map.put("Message","Get Data Unsuccessfully");
        }
        return map;
    }




    @GetMapping(value = "/countAllUserInGroup/{groupId}",produces = {"application/json"})
    public Map<String,Object> countAllUserInGroup(@PathVariable("groupId") int groupId)
    {
        Map<String,Object> map = new HashMap<>();
       int user = this.userService.countAllUserInGroup(groupId);

        if(user > 0)
        {
            map.put("Status",true);
            map.put("Data",user);
            map.put("Message","Get Data Successfully");

        }else
        {
            map.put("Status",false);
            map.put("Message","Get Data Unsuccessfully");
        }
        return map;
    }

    @GetMapping(value = "/countNotificationByTargetUserId/{target_user_id}",produces = {"application/json"})
    public Map<String,Object> countNotificationByTargetUserId(@PathVariable("target_user_id") int target_user_id)
    {
        Map<String,Object> map = new HashMap<>();
        int user = this.userService.countNotificationByTargetUserId(target_user_id);

        if(user > 0)
        {
            map.put("Status",true);
            map.put("Data",user);
            map.put("Message","Get Data Successfully");

        }else
        {
            map.put("Status",false);
            map.put("Message","Get Data Unsuccessfully");
        }
        return map;
    }


    @GetMapping(value = "/findMatesByUserId/{userId}",produces = {"application/json"})
    public Map<String,Object> findMatesByUserId(@PathVariable("userId") int userId)
    {
        Map<String,Object> map = new HashMap<>();
        List<User> user = this.userService.findMatesByUserId(userId);

        if(!user.isEmpty())
        {
            map.put("Status",true);
            map.put("Data",user);
            map.put("Message","Get Data Successfully");

        }else
        {
            map.put("Status",false);
            map.put("Message","Get Data Unsuccessfully");
        }
        return map;
    }


}

