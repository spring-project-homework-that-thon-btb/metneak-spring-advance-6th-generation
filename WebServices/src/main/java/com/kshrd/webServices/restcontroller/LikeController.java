package com.kshrd.webServices.restcontroller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kshrd.models.Like;
import com.kshrd.services.LikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/like")
public class LikeController {
    private HttpStatus httpStatus ;
    private Map<String,Object> map;

    @Autowired
    private LikeService likeService;

    //Save like to mn_like
    @PostMapping("/{userId}/{postId}")
    public Map<String,Object> saveLike(@PathVariable("userId") int userId,@PathVariable("postId") int postId){
        map=new HashMap<>();
        if (likeService.saveLike(userId,postId)==1){
            map.put("message","Save like is successfully!!!");
            map.put("status",true);
        }
        else {
            map.put("message","Save like is not successfully!!!");
            map.put("status",false);
        }

        return  map;
    }

    //Count like
    @GetMapping("/count/{postId}")
    public Map<String,Object> countLike(@PathVariable("postId") int postId){
        map=new HashMap<>();
        if (likeService.countLike(postId)>=0){
            map.put("message","Count Like By Post Id is successfully!!!");
            map.put("status",true);
            map.put("TotalCount",likeService.countLike(postId));
        }

        return map;
    }

    //Get All like
    @GetMapping(value = "/post/{postId}/status/{status}",produces = {"application/json"})
    public Map<String,Object> getAllLike(@PathVariable("postId") Integer postId,@PathVariable("status") Boolean status) {
        //Pageable pageable=new PageRequest(0,5);
        Map<String,Object> maps = new HashMap<>();

        List<Like> likeList=likeService.getAllLikes(postId,status);

        if(!likeList.isEmpty()){
            maps.put("message","Get All like is successfully!!!");
            maps.put("status",true);
            maps.put("data",likeList);
        }
        else {
            maps.put("message","Get All like is not successfully!!!");
            maps.put("status",false);

        }
        return maps;
    }

    @PutMapping(value = "delete/{userId}/{postId}")
    public Map<String,Object> deleteLike(@PathVariable("userId") int userId,@PathVariable("postId") int postId){
        map=new HashMap<>();
        if (likeService.deleteLike(userId,postId)==1){
            map.put("message","Delete like is successfully!!!");
            map.put("status",true);
        }
        else {
            map.put("message","Delete like is not successfully!!!");
            map.put("status",false);
        }
        return  map;
    }
}