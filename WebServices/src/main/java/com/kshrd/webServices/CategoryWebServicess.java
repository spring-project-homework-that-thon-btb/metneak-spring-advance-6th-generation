package com.kshrd.webServices;

import com.kshrd.models.Category;
import com.kshrd.services.CategoryServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CategoryWebServicess {

    @Autowired
    private CategoryServices categoryServices;

    @GetMapping("/a")
    public List<Category> categories(){
        System.out.println("Runed WebService");
        System.out.println(categoryServices.allCategory().get(0));
        return categoryServices.allCategory();
    }
}
