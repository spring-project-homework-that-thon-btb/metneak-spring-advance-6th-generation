package com.kshrd.webServices.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.util.Base64;

@Configuration
@EnableWebSecurity
//@Order()
public class APIConfiguration extends WebSecurityConfigurerAdapter {


    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("spring_user").password(passwordEncoder.encode("spring_pass")).roles("API");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http
                .authorizeRequests()
                .antMatchers("/css/style.css").permitAll()
                .antMatchers("/api/**").hasAnyRole("API").anyRequest().authenticated()
                .and().formLogin().loginPage("/login")
                .permitAll().and().logout().permitAll();


        http.httpBasic();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

        public static void main(String arg[]){

            String user="spring_user";
            String pass="spring_pass";
            String bassic= Base64.getUrlEncoder().encodeToString((user+ ":" + pass).getBytes());

            System.out.println(bassic);
        }

}
