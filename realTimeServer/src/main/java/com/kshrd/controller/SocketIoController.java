package com.kshrd.controller;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIONamespace;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.DataListener;
import com.kshrd.models.Comment;
import com.kshrd.models.FriendRequest;
import com.kshrd.models.Notification;
import com.kshrd.models.Post;
import javafx.geometry.Pos;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SocketIoController {

    private Logger logger = LogManager.getLogger();

    private SocketIONamespace socketIONamespace;

    public SocketIONamespace getSocketIONamespace() {
        return socketIONamespace;
    }

    //call socket server
    // and create listener event
    @Autowired
    public SocketIoController(SocketIOServer server) {
        this.socketIONamespace = server.addNamespace("/action");

        // comment on post
        this.socketIONamespace.addEventListener("comment", Comment.class, onComment);

        //like on post
        this.socketIONamespace.addEventListener("like", Integer.class, onLike);

        // when post to a group
        this.socketIONamespace.addEventListener("postGroup", Post.class, onPostGroup);

        // when post to a mate
        this.socketIONamespace.addEventListener("postMate", Post.class, onPostMate);

        // when user addFriend
        this.socketIONamespace.addEventListener("addFriend", FriendRequest.class, onAddFriend);

        // when user confirm
        this.socketIONamespace.addEventListener("confirm", Notification.class, onConfirm);

        // when user delete post
        this.socketIONamespace.addEventListener("deletePost", Post.class, onDeletePost);

        // when user delete post
        this.socketIONamespace.addEventListener("editPost", Post.class, onEditPost);
        // add friend//
//        this.socketIONamespace.addEventListener("addFriend",);

        // confirm friend//

        // delete post

        // update post

        // user join


    }
    //ondelet Post
    public  DataListener<Post> onDeletePost=new DataListener<Post>() {
        @Override
        public void onData(SocketIOClient socketIOClient, Post post, AckRequest ackRequest) throws Exception {
            socketIONamespace.getBroadcastOperations().sendEvent("onDeletePost",socketIOClient,post);
        }
    };
    //on edite Post
    public  DataListener<Post> onEditPost=new DataListener<Post>() {
        @Override
        public void onData(SocketIOClient socketIOClient, Post post, AckRequest ackRequest) throws Exception {
            socketIONamespace.getBroadcastOperations().sendEvent("onEditPost",socketIOClient,post);
        }
    };
   //confirm
   public  DataListener<Notification> onConfirm=new DataListener<Notification>() {
       @Override
       public void onData(SocketIOClient socketIOClient, Notification Notification, AckRequest ackRequest) throws Exception {
           socketIONamespace.getBroadcastOperations().sendEvent("onConfirm",socketIOClient,Notification);
       }
   };


    // add friend
    public  DataListener< FriendRequest> onAddFriend=new DataListener<FriendRequest>()  {
            @Override
            public void onData(SocketIOClient socketIOClient, FriendRequest friendRequest, AckRequest ackRequest) throws Exception {
                socketIONamespace.getBroadcastOperations().sendEvent("onAddFriend",socketIOClient,friendRequest);
            }
        };

    // comment
    public DataListener<Comment> onComment = new DataListener<Comment>() {
        @Override
        public void onData(SocketIOClient socketIOClient, Comment comment, AckRequest ackRequest) throws Exception {
            socketIONamespace.getBroadcastOperations().sendEvent("onComment", socketIOClient, comment);
            logger.info(comment);
        }
    };

    // total like
    public DataListener<Integer> onLike = new DataListener<Integer>() {
        @Override
        public void onData(SocketIOClient socketIOClient, Integer totalLike, AckRequest ackRequest) throws Exception {
            socketIONamespace.getBroadcastOperations().sendEvent("onLike",socketIOClient,totalLike);
            logger.info(totalLike);
        }
    };

    //When Post to a group
    public DataListener<Post> onPostGroup=new DataListener<Post>() {
        @Override
        public void onData(SocketIOClient socketIOClient, Post post, AckRequest ackRequest) throws Exception {
            socketIONamespace.getBroadcastOperations().sendEvent("onPostGroup",socketIOClient,post);
        }
    };

    //When Post to a mate
    public DataListener<Post> onPostMate=new DataListener<Post>() {
        @Override
        public void onData(SocketIOClient socketIOClient, Post post, AckRequest ackRequest) throws Exception {
            socketIONamespace.getBroadcastOperations().sendEvent("onPostMate",socketIOClient,post);
        }
    };




    public void broadcastEvent(String event, Object data) {
        this.getSocketIONamespace().getBroadcastOperations().sendEvent(event, data);
        logger.info(data);
    }

    public void stopServer() {
        this.stopServer();
    }
}
