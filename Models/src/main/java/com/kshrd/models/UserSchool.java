package com.kshrd.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Table;
@Entity
@Table(name = "mn_user_school")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class UserSchool implements Serializable {

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "first",column = @Column(name = "user_id")),
            @AttributeOverride(name = "second",column = @Column(name = "school_id"))
    })
    private PrimaryId primaryId;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("first")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("second")
    private School school;

    private Timestamp startedYear;
    private Timestamp graduatedYear;
    private Timestamp createdDate;
    private Timestamp modifiedDate;
    private Boolean status;

    @Override
    public String toString() {
        return "UserSchool{" +
                "primaryId=" + primaryId +
                ", user=" + user +
                ", school=" + school +
                ", startedYear=" + startedYear +
                ", graduatedYear=" + graduatedYear +
                ", createdDate=" + createdDate +
                ", modifiedDate=" + modifiedDate +
                ", status=" + status +
                '}';
    }

    public UserSchool() {
    }

    public PrimaryId getPrimaryId() {
        return primaryId;
    }

    public void setPrimaryId(PrimaryId primaryId) {
        this.primaryId = primaryId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public Timestamp getStartedYear() {
        return startedYear;
    }

    public void setStartedYear(Timestamp startedYear) {
        this.startedYear = startedYear;
    }

    public Timestamp getGraduatedYear() {
        return graduatedYear;
    }

    public void setGraduatedYear(Timestamp graduatedYear) {
        this.graduatedYear = graduatedYear;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public UserSchool(PrimaryId primaryId, User user, School school, Timestamp startedYear, Timestamp graduatedYear, Timestamp createdDate, Timestamp modifiedDate, Boolean status) {
        this.primaryId = primaryId;
        this.user = user;
        this.school = school;
        this.startedYear = startedYear;
        this.graduatedYear = graduatedYear;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.status = status;
    }
}
