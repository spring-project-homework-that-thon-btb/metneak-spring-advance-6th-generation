package com.kshrd.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "mn_user")
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String firstName;
    private String lastName;
    private String username; // old version is userName
    @Column(length = 1)
    private String gender;
    private Timestamp dob;
    private String email;
    private String facebookId;
    private Timestamp createDate;
    private Timestamp modifiedDate;
    private String description;
    @Column(length = 1, columnDefinition = "varchar(1) default 'U'")
    private String role;
    private String profileUrl;
    private String coverUrl;
    private Boolean status;
    @JsonProperty("is_active")
    private Boolean isActive;
    @Transient
    private List<School> schools;

    public List<School> getSchools() {
        return schools;
    }

    public void setSchools(List<School> schools) {
        this.schools = schools;
    }

    @JsonIgnore
    @OneToMany(
            mappedBy = "user",
            orphanRemoval = true,
            cascade = CascadeType.ALL
    )
    private List<UserSchool> userSchools;

    @JsonIgnore
    @ManyToOne(
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            optional = true
    )
    @JoinColumn(name = "location_id")
    private Location locations;


    @JsonIgnore
    @OneToMany(
            mappedBy = "user",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL
    )
    private List<MateList> mateLists;


    @JsonIgnore
    @OneToMany(
            mappedBy = "user",
            fetch = FetchType.LAZY,
            orphanRemoval = true,
            cascade = CascadeType.ALL
    )
    private List<UserGroup> userGroups;


    @JsonIgnore
    @OneToMany(
            fetch = FetchType.EAGER,
            orphanRemoval = true
    )
    private List<Post> posts;


    @JsonIgnore
    @OneToMany(
            mappedBy = "user",
            fetch = FetchType.LAZY,
            orphanRemoval = true
    )
    private List<Comment> comments;


    @JsonIgnore
    @OneToMany(
            mappedBy = "user",
            fetch = FetchType.LAZY,
            orphanRemoval = true
    )
    private List<Notification> notifications;


    @JsonIgnore
    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL
    )
    private List<ReadNotification> readNotifications;

    public User() {
    }


    public User(String firstName, String lastName, String username, String gender, Timestamp dob, String email, String facebookId, Timestamp createDate, Timestamp modifiedDate, String description, String role, String profileUrl, String coverUrl, Boolean status, Boolean isActive, Location locations) {
        this.lastName = lastName;
        this.username = username;
        this.gender = gender;
        this.dob = dob;
        this.email = email;
        this.facebookId = facebookId;
        this.createDate = createDate;
        this.modifiedDate = modifiedDate;
        this.description = description;
        this.role = role;
        this.profileUrl = profileUrl;
        this.coverUrl = coverUrl;
        this.status = status;
        this.isActive = isActive;
//        this.userSchools = userSchools;
        this.locations = locations;
//        this.mateLists = mateLists;
//        this.userGroups = userGroups;
//        this.posts = posts;
//        this.comments = comments;
//        this.notifications = notifications;
//        this.readNotifications = readNotifications;
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", username='" + username + '\'' +
                ", gender='" + gender + '\'' +
                ", dob=" + dob +
                ", email='" + email + '\'' +
                ", facebookId='" + facebookId + '\'' +
                ", createDate=" + createDate +
                ", modifiedDate=" + modifiedDate +
                ", description='" + description + '\'' +
                ", role='" + role + '\'' +
                ", profileUrl='" + profileUrl + '\'' +
                ", coverUrl='" + coverUrl + '\'' +
                ", status=" + status +
                ", isActive=" + isActive +
                ", userSchools=" + userSchools +
                ", locations=" + locations +
                ", mateLists=" + mateLists +
                ", userGroups=" + userGroups +
//                ", posts=" + posts +
                ", comments=" + comments +
                ", notifications=" + notifications +
                ", readNotifications=" + readNotifications +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Timestamp getDob() {
        return dob;
    }

    public void setDob(Timestamp dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public List<UserSchool> getUserSchools() {
        return userSchools;
    }

    public void setUserSchools(List<UserSchool> userSchools) {
        this.userSchools = userSchools;
    }

    public Location getLocations() {
        return locations;
    }

    public void setLocations(Location locations) {
        this.locations = locations;
    }

    public List<MateList> getMateLists() {
        return mateLists;
    }

    public void setMateLists(List<MateList> mateLists) {
        this.mateLists = mateLists;
    }

    public List<UserGroup> getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(List<UserGroup> userGroups) {
        this.userGroups = userGroups;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    public List<ReadNotification> getReadNotifications() {
        return readNotifications;
    }

    public void setReadNotifications(List<ReadNotification> readNotifications) {
        this.readNotifications = readNotifications;
    }
}
