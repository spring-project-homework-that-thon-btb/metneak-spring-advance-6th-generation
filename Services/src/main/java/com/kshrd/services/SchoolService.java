package com.kshrd.services;

import com.kshrd.models.School;

import java.util.List;

public interface SchoolService {

     List<School> getall();
     List<School> getAllHighSchools();
     List<School> getAllSecondaySchools();
     List<School> getAllPrimarySchools();

}
