package com.kshrd.services.serviceImpl;

import com.kshrd.models.School;
import com.kshrd.repositories.SchoolRepository;
import com.kshrd.services.SchoolService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class SchoolServiceImpl implements SchoolService {

    public SchoolServiceImpl(SchoolRepository schoolRepository) {
        this.schoolRepository = schoolRepository;
    }

    private SchoolRepository schoolRepository;



    @Override
    public List<School> getall() {
        return this.schoolRepository.getall();
    }

    @Override
    public List<School> getAllHighSchools() {
        List<School> schools = this.schoolRepository.findAllSchools();
       List<School> schoolList = schools.stream().filter(x->x.getLevel().equals("h")).collect(Collectors.toList());
        return schoolList;
    }

    @Override
    public List<School> getAllSecondaySchools() {
        List<School> schools = this.schoolRepository.findAllSchools();
        List<School> schoolList = schools.stream().filter(school -> school.getLevel().equals("s")).collect(Collectors.toList());
        return schoolList;
    }

    @Override
    public List<School> getAllPrimarySchools() {
        List<School> schools = this.schoolRepository.findAllSchools();
        List<School> schoolList = schools.stream().filter(school -> school.getLevel().equals("p")).collect(Collectors.toList());
        return schoolList;
    }
}
