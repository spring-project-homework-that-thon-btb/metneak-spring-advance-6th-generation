package com.kshrd.services.serviceImpl;

import com.kshrd.models.OneSignalPushNotifcation;
import com.kshrd.repositories.OneSignalPushNotificationRepository;
import com.kshrd.services.OneSignalPushNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class OneSignalPushNotificationServiceImpl implements OneSignalPushNotificationService {

    @Autowired
    private OneSignalPushNotificationRepository oneSignalPushNotificationRepository;


    // saveDevinces OneSigna


    @Override
    public Integer saveOneDevice(Integer userId, String playerId) {
        return oneSignalPushNotificationRepository.saveOneDevice(userId,playerId);
    }

    @Override
    public OneSignalPushNotifcation findOneDevice(String playerId) {
        return oneSignalPushNotificationRepository.findOneSignalPushNotifcationByPlayerId(playerId);
    }

    @Override
    public List<String> findDeviceByUserId(Integer userId) {
        return oneSignalPushNotificationRepository.findOneSignalPushNotifcationByUserId(userId);
    }

    @Override
    public List<String> findOneSignalPushNotifcationByGroupId(Integer gId) {
        return oneSignalPushNotificationRepository.findOneSignalPushNotifcationByGroupId(gId);
    }

    @Override
    public List<String> findOneSignalPushNotifcationForComment(Integer postId, Integer userId) {
        return oneSignalPushNotificationRepository.findOneSignalPushNotifcationForComment(postId,userId);
    }
}
