package com.kshrd.services.serviceImpl;


import com.kshrd.models.Location;
import com.kshrd.repositories.LocationRepository;
import com.kshrd.services.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationServiceImpl implements LocationService {


    private LocationRepository locationRepository;

    @Autowired
    public LocationServiceImpl(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    @Override
    public List<Location> getallprovince() {
        return this.locationRepository.getallprovice();
    }


    @Override
    public List<Location> findDistrictNameByProvinceName(String provinceName) {
        return this.locationRepository.findDistrictNameByProvinceName(provinceName);
    }


}
