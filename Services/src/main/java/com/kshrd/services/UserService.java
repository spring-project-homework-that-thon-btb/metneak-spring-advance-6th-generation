package com.kshrd.services;

import com.kshrd.models.User;
import com.kshrd.models.from.FrmsocailMedia;

import java.util.List;

public interface UserService {




    List<User> getall();
    int countAllUser();
    List<User>  findUserByFacebookId(String facebookId);
    boolean  checkUserByFacebookId(String facebookId);
    List<User> findUserById(int userId);
    //    List<User> findAllByEmail(String email);
    List<User> findAllUserFriendByUserId(int userId);
    int countAllUserFriend(int user_id);
    List<User> findAllUserInGroup(int groupId);
    int countAllUserInGroup(int groupId);
    int countNotificationByTargetUserId(int target_user_id);
    List<User> findMatesByUserId(int userId);
    List insertUpdateRemember(int userId, int mateId, boolean status);
    int checkuserandmatebyid(int userId, int mateId);
    List<String> checkUserReadNotification(int userId, int notificationId);
    List<String> insertuserreadnotification(int notificationId,int userId);
    boolean saveuser(User user);
    List<User> findUserbyEmail(String email);
    boolean saveUserSocailmedia(FrmsocailMedia frmsocailMedia);
    boolean updateUserPhoto(String profileUrl, String coverUrl,int userId);
    int countNotificationByTargetGroupId(int userId);
    int countAllUserBySchoolAndLevel(String school,String level);
    int updateIsReadNotification(int userId);
    List<User> findAllUserBySchoolAndLevel(String school,String level);
    boolean updateUserById(User user);
    boolean registerUser(User user);

}
