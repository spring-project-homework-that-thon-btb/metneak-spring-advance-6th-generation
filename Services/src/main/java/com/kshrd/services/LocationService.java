package com.kshrd.services;

import com.kshrd.models.Location;

import java.util.List;

public interface LocationService {

    List<Location> getallprovince();
    List<Location> findDistrictNameByProvinceName(String provinceName);
}
